package at.hakwt.swp4.cohesion;

public class CohesionMain {

    public static void main(String[] args) {
        Car car = new Car();
        Garage garage = new Garage();
        Chef chef = new Chef();
        Kitchen kitchen = new Kitchen();
        Traveler traveler = new Traveler(car, chef);

        garage.maintainCar(traveler.getCar());

        kitchen.cook(traveler.getChef());

        traveler.startJourney();

    }

}
