package at.hakwt.swp4.cohesion;

public class Traveler {

    private final Car car;

    private final Chef chef;

    public Traveler(Car car, Chef chef) {
        this.car = car;
        this.chef = chef;
    }

    public void startJourney() {
        // ...
    }

    public Car getCar() {
        return car;
    }

    public Chef getChef() {
        return chef;
    }
}
