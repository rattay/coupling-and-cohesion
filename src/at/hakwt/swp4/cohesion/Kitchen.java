package at.hakwt.swp4.cohesion;

public class Kitchen {
    public Food cook(Chef chef) {
        chef.buy();
        chef.cook();
        return new Food();
    }
}
