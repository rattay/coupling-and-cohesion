package at.hakwt.swp4.coupling;

public abstract class Vehicle {

    public abstract void move();
}
