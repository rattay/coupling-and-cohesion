package at.hakwt.swp4.coupling;

public class CouplingMain {

    public static void main(String[] args) {
        Vehicle v = new Car();
	    Traveler t = new Traveler(v);
	    t.startJourney();

        Vehicle bike = new Bicycle();
        Traveler bikeTraveler = new Traveler(bike);
        bikeTraveler.startJourney();
    }
}
