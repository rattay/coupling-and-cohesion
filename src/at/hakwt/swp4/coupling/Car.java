package at.hakwt.swp4.coupling;

import at.hakwt.swp4.coupling.Vehicle;

public class Car extends Vehicle {

    @Override
    public void move() {
        System.out.println("car moving");
    }
}