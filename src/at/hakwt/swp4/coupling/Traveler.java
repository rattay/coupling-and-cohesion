package at.hakwt.swp4.coupling;

public class Traveler {

    private Vehicle v;

    public Traveler(Vehicle v) {
        this.v = v;
    }

    public void startJourney() {
        v.move();
    }

}
