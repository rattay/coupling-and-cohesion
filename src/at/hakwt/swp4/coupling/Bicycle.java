package at.hakwt.swp4.coupling;

public class Bicycle extends Vehicle{

    @Override
    public void move() {
        System.out.println("bicycle moving");
    }
}
